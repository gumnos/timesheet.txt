#!/bin/bash

# Get the directory the script is stored in.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Set where the user config should come from.
CONFIG=$HOME/.timesheet.conf

# Check for the conf file.
if [ ! -f $CONFIG ]; then
  # If there is no timesheet in the $USER dir 
  # we use the default.
  CONFIG=$DIR/timesheet.conf
fi

# Get the configs.
source $CONFIG

# Make the timesheet directory.
mkdir -p $TIMESHEET_DIR

# Then we need a few more directories.
mkdir -p $TIMESHEET_DIR/timesheets

# Create the timesheet projects file.
if [ ! -f $TIMESHEET_DIR/projects.txt ]; then
  echo "@${USER}:project_${USER}.txt" >> $TIMESHEET_DIR/projects.txt
  chmod u+x $TIMESHEET_DIR/projects.txt
fi

function load_time_file() {
  # Get the timesheet file for the project.
  local TIME_FILE=$(grep $1 $TIMESHEET_DIR/projects.txt)
  IFS=':' read -a arr <<< "$TIME_FILE"
  local TIME_FILE="${TIMESHEET_DIR}/timesheets/${arr[1]}"

  echo $TIME_FILE
}

# Get the flags.
case $1 in
  -h|--help)
    echo "Timesheet - Manage timesheets per project from the command line"
    echo " "
    echo "timesheet [options]"
    echo " "
    echo "options:"
    echo "-l, --list-projects       list all projects"
    echo "-p, --new-project         create a new project"
    echo "-t, --log-time            create a new time entry"
    echo "-f, --find-time           find times using a search term"
    echo "-c, --calculate           calculate running total, hours and money"
    echo "-r, --report              generate a report on all projects with a specific search term"
    echo "-E, --export-all          export all projects by search term"
    echo "-h, --help                this help text"
    echo " "
    echo "examples:"
    echo "create a new project:"
    echo "timesheet -p [project_name] [file_name]"
    echo "timesheet -p @project_name project_file.txt"
    echo " "
    echo "create a new time entry:"
    echo "timesheet -t [project_name] [date] [hours] [comment]"
    echo "timesheet -t @project_name \"04/11/2015\" 2 \"This is a comment\""
    echo " "
    echo "find times using a search term"
    echo "timesheet -c [project_name] [search_term] [hourly rate]"
    echo "timesheet -c @project_name 11/2015 350"
    echo " "
    echo "generate report using a search term"
    echo "timesheet -r [search_term]"
    echo "timesheet -r 04/2019"
    echo ""
    echo "export all time into tab separated file by search term"
    echo "timesheet -E [search_term]"
    echo "timesheet -E 04/2019"
    echo ""
    ;;
  -l|--list-projects)
    echo ""
    cat $TIMESHEET_DIR/projects.txt
    echo ""
    ;;
  -p|--new-project)
    PROJ=`echo "${2}" | tr -d '@'`
    THE_DATE=`date +'%d/%m/%Y'`
    echo "@${PROJ}:project_${PROJ}.txt" >> $TIMESHEET_DIR/projects.txt

    echo ""
    echo "Created new project ${PROJ}"
    echo ""
    echo "Alias: @${PROJ}"
    echo "File: ${TIMESHEET_DIR}/timesheets/project_${PROJ}.txt"
    echo ""
    echo "Log time to this project"
    echo "Example: timesheet -t @${PROJ} ${THE_DATE} 2 'Writing awesome codes'"
    echo ""
    ;;
  -t|--log-time)
    TIME_FILE=$(load_time_file $2)

    # Put the new time into the project timesheet.
    echo -e "$3\t$4\t$5" >> $TIME_FILE
    ;;
  -f|--find-time)
    TIME_FILE=$(load_time_file $2)

    echo " "
    cat $TIME_FILE |grep $3
    echo " "
    ;;
  -c|--calculate)
    TIME_FILE=$(load_time_file $2)

    TMP_FILE=/tmp/$RANDOM
    grep $3 $TIME_FILE >> $TMP_FILE

    HOUR_CALC=0

    while IFS='' read -r line || [[ -n "$line" ]]; do
      IFS=$'\t' read -a hrs <<< "$line"
      HOUR_CALC=$(($HOUR_CALC+${hrs[1]}))
    done < "$TMP_FILE"
    
    $DIR/timesheet -f $2 $3
    echo "Hours: $HOUR_CALC"
    echo "Hourly rate: $4"
    echo "Running total: $(($HOUR_CALC*$4))"
    echo " "

    rm -f $TMP_FILE
    ;;
  -r|--report)
    # Report on all projects based on a search term.
    while IFS='' read -r line || [[ -n "$line" ]]; do
      IFS=':' read -a arr <<< "$line"
      
      if [ -f ${TIMESHEET_DIR}/timesheets/${arr[1]} ]; then
        echo ${arr[0]}
        $DIR/timesheet -f ${arr[0]} $2
      fi

    done < "$TIMESHEET_DIR/projects.txt"
    ;;
  -E|--export-all)
    while IFS='' read -r line || [[ -n "$line" ]]; do
      IFS=':' read -a arr <<< "$line"
      
      if [ -f ${TIMESHEET_DIR}/timesheets/${arr[1]} ]; then        
        while IFS=$'\t' read -r time_line || [[ -n "$time_line" ]]; do
          echo -e "${arr[0]}\t${time_line}"
        done < ${TIMESHEET_DIR}/timesheets/${arr[1]} |grep $2
      fi

    done < "$TIMESHEET_DIR/projects.txt"
    ;;
  *)
    # If an unknown command is sent call the help.
    $DIR/timesheet --help
    ;;
esac
