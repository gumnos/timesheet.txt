# Timesheet

Manage timesheets per project from the command line

## Screenshots

![The help text when running timesheet without any parameters or with -h or --help](https://i.imgur.com/q9IWMfH.png)

The help text when running timesheet without any parameters or with -h or --help

![Creating a new project. The output lists the alias to use when referencing the project as well as the tab delimited text file. It also gives some helpful example text.](https://i.imgur.com/4mm7RoL.png)

Creating a new project. The output lists the alias to use when referencing the project as well as the tab delimited text file. It also gives some helpful example text.

![Listing all projects. Timesheet.txt automatically creates a project using your username.](https://i.imgur.com/h1vYSP0.png)

Listing all projects. Timesheet.txt automatically creates a project using your username.

![Adding a time entry to your project.](https://i.imgur.com/oqWfG5I.png)

Adding a time entry to your project.

![Calculating hours and total cost for a project and search term including an hourly rate.](https://i.imgur.com/6trVOme.png)

Calculating hours and total cost for a project and search term including an hourly rate.

![Generating a full report based on a search term for all projects.](https://i.imgur.com/AT7J90n.png)

Generating a full report based on a search term for all projects.

![Generating a report and saving it to a tab delimited TXT file to be used in other applications such as Excel or Libreoffice Calc](https://i.imgur.com/JlwuYrC.png)

Generating a report and saving it to a tab delimited TXT file to be used in other applications such as Excel or Libreoffice Calc

![Imported into Excel](https://i.imgur.com/cKQ8Pih.png)

Imported into Excel

## System requirements

Timesheet.txt has been tested on MacOS and Ubuntu. It should work on pretty much all flavours of Linux. Windows should work using Git Bash or Cygwin but this has not been tested. If this becomes a requirement I might rewrite this tool in Powershell for Windows.

## Installation

Copy `timesheet.conf` to `~/.timesheet.conf`. You can change the `TIMESHEET_DIR` variable to any path you like, this is where your projects and timesheets will be stored.

Create a shortcut: `sudo ln -s /home/[path]/timesheet.txt/timesheet /usr/local/bin/timesheet`

## Basics

This tool just manages simple text files. So it is easy to manually add or remove projects/time entries. You could also set your `TIMESHEET_DIR` to be in your Dropbox or file sharing folder and access your timesheets anywhere. Your timesheets could also be stored in Git for example.

The projects.txt file is store in `TIMESHEET_DIR`/projects.txt and is formatted: `@project_name`:`project_file.txt`. Each project is on it's own line.

The timesheets all live in their own project files in `TIMESHEET_DIR`/timesheets. These are also just text files and each column is tab delimited. Each row is on it's own line. This makes the file easy to read and also easy to import into other applications for further reporting, invoicing etc.

The timesheet file is broken up into the following colums:

`Date`	`Hours`	`Comment`

All of these files can be manually edited without breaking the timesheet application.

## Usage

timesheet [options]

### Options:

* -l, --list-projects       list all projects
* -p, --new-project         create a new project
* -t, --log-time            create a new time entry
* -f, --find-time           find times using a search term
* -c, --calculate           calculate running total, hours and money
* -r, --report              generate a report on all projects with a specific search term
* -E, --export-all          export all projects by search term
* -h, --help                this help text

### Examples:

#### Create a new project:

* timesheet -p [project_name] [file_name]
* timesheet -p @project_name project_file.txt

#### Create a new time entry:

* timesheet -t [project_name] [date] [hours] [comment]
* timesheet -t @project_name 04/11/2015 2 "This is a comment"

#### Find times using a search term

* timesheet -c [project_name] [search_term] [hourly rate]
* timesheet -c @project_name 11/2015 350

#### Generate report using a search term

* timesheet -r [search_term]
* timesheet -r 04/2019

#### Export all time into tab separated file by search term

* timesheet -E [search_term] > [file to save export to]
* timesheet -E 04/2019 > AprilExport.tsv